/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package buscador;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author rsaaperez
 */
public class Metodos {

    //Creamos un metodo para que el usuario pueda añadir sus propias cartas.
    public void insertar(ArrayList<Cartas> cartas) {
        String pnombre = JOptionPane.showInputDialog("Introduce el nombre de la carta que deseas añadir:");
        String pmana = JOptionPane.showInputDialog("Introduce el coste de mana convertido de la carta que deseas añadir:");
        int pcmc = Integer.parseInt(pmana);
        String pcolor = JOptionPane.showInputDialog("Introduce el color de la carta que deseas añadir:");
        String ptipo = JOptionPane.showInputDialog("Introduce el tipo de la carta que deseas añadir:");
        String prareza = JOptionPane.showInputDialog("Introduce la rareza de la carta que deseas añadir:");
        String phabilidad = JOptionPane.showInputDialog("Introduce la habilidad de la carta que deseas añadir si la tiene:");
        cartas.add(new Cartas(pnombre, pcmc, pcolor, ptipo, prareza, phabilidad));
        JOptionPane.showMessageDialog(null, "Carta introducida en nuestra base de datos.");
    }
    //Creamos un metodo que lea un fichero que contenga una serie de cartas.
    public static void leerLista(File fichero) {
        Scanner sc = null;
        ArrayList<Cartas> lista = new ArrayList<>();
        try {
            sc = new Scanner(fichero);
            while (sc.hasNext()) {
                String persona = sc.nextLine();
                String datos[] = persona.split(",");
                Cartas c = new Cartas(datos[0],Integer.parseInt(datos[1]),datos[2],datos[3],datos[4],datos[5]);
                lista.add(c);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error" + e.getMessage());
        } finally {
            if (sc != null) {
                sc.close();
            }
            for(int i =0;i<lista.size();i++){
                System.out.println(lista.get(i));
            }
        }
    }
}
