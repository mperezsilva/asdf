/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package buscador;

/**
 *
 * @author rsaaperez
 */
public class Cartas {
    //Creamos los elementos privados que necesitaremos introducir en nuestro Array de cartas.
    private String nombre;
    private int cmc;
    private String color;
    private String tipo;
    private String rareza;
    private String habilidad;

    //Creamos los constructores de nuestra clase.
    public Cartas() {
    //Vacio.
    }

    public Cartas(String nombre, int cmc, String color, String tipo, String rareza, String habilidad) {
        this.nombre = nombre;
        this.cmc = cmc;
        this.color = color;
        this.tipo = tipo;
        this.rareza = rareza;
        this.habilidad = habilidad;
    }

    //Generamos los getters  y los setters necesarios para manejar nuestros elementos
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCmc() {
        return cmc;
    }

    public void setCmc(int cmc) {
        this.cmc = cmc;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getRareza() {
        return rareza;
    }

    public void setRareza(String rareza) {
        this.rareza = rareza;
    }

    public String getHabilidad() {
        return habilidad;
    }

    public void setHabilidad(String habilidad) {
        this.habilidad = habilidad;
    }
    
}
